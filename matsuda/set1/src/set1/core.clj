(ns set1.core
  (:use [clojure.java.io :as io])
  (:use [clojure.string :as str]))

(load "1")
(load "2")
(load "5")

(defn -main
  [& args]
  ;001
  (wc (java.io.BufferedReader. *in*))
  ;002
  (replace-tab (java.io.BufferedReader. *in*))
  ;003
  ;(split-tab)
  ;005
  (head (java.io.BufferedReader. *in*) (doseq [arg *command-line-args*]))
  )
