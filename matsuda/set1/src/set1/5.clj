(in-ns 'set1.core)

(defn head
  [lines number]
  (loop [x 0 l (.readLine lines)]
    (when (< x number)
      (println l)
      (recur (inc x) (.readLine lines))))
  )
