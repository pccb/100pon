(in-ns 'set1.core)

(defn replace-tab
  [lines]
  (doseq [line (line-seq lines)]
    (println (str/replace line #"\s" " "))))
