let head a_int =
  let rec head_sub l_int =
    let m_string, n_bool = try read_line (), false with End_of_file -> "", true in
    match l_int with
      0 -> (ignore 1)
    | _ ->
      if n_bool then (ignore 1)
      else
	(print_endline m_string;
	 head_sub (l_int - 1))
  in
  head_sub a_int
;;

head (int_of_string Sys.argv.(1));;
