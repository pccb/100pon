(* 
   Strモジュールを新たに読み込む必要があるので、$ ocaml 2.ml では実行できない。
   $ ocamlopt str.cmxa 2.ml -o "実行ファイル名" でコンパイルする等する必要がある
   詳しくは -> http://ocaml.jp/Chapter%2023%20The%20str%20library%EF%BC%9A%20%E6%AD%A3%E8%A6%8F%E8%A1%A8%E7%8F%BE%E3%81%A8%E6%96%87%E5%AD%97%E5%88%97%E5%87%A6%E7%90%86
   ocamlc:str.cma, ocamlopt:str.cmxa
*)
   
open Str;;

(* channel *)
let col1 = open_out "col1.txt";;
let col2 = open_out "col2.txt";;
let col_list = [col1; col2];;

(* 書き込むだけのfunction *)
let rec write_fun a_channel_list b_string_list =
  match (a_channel_list, b_string_list) with
    ([], []) -> ignore 1
  | (c_channel :: d_channel_list, [] ) ->
    output_string c_channel "\n";
    write_fun d_channel_list []
  | (c_channel :: d_channel_list, e_string :: f_string_list) ->
    output_string c_channel (e_string ^ "\n");
    write_fun d_channel_list f_string_list
  | _ ->
    print_endline "error";
    exit(1)
;;
      

(* main *)
let wc a_channel_list =
  let rec wc_def l_int =
    let m_string, n_bool = try read_line (), false with End_of_file -> "", true in
    write_fun a_channel_list (split (regexp "\t") m_string);    
    if n_bool then l_int
    else wc_def (l_int + 1)
  in
  wc_def 0
;;

Printf.printf "%d\n" (wc col_list);;
close_out col1;
close_out col2;
    
    
      
