let tail a_int =
  let rec pr_list_n l_string_list m_int =
    if m_int <= 0 then ignore 1
    else
      match l_string_list with
	[] -> print_endline "list is empty"
      | n_string :: o_string_list ->
	pr_list_n o_string_list (m_int - 1);
	print_endline n_string
  in	    
  
  let rec tail_sub l_string_list =
    let m_string, n_bool = try read_line (), false with End_of_file -> "", true in
      if n_bool then pr_list_n l_string_list a_int
      else
	tail_sub (m_string :: l_string_list)
  in
  tail_sub []
;;

tail (int_of_string Sys.argv.(1));;
