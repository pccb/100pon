(* 
   Strモジュールを新たに読み込む必要があるので、$ ocaml 2.ml では実行できない。
   $ ocamlopt str.cmxa 2.ml -o "実行ファイル名" でコンパイルする等する必要がある
   詳しくは -> http://ocaml.jp/Chapter%2023%20The%20str%20library%EF%BC%9A%20%E6%AD%A3%E8%A6%8F%E8%A1%A8%E7%8F%BE%E3%81%A8%E6%96%87%E5%AD%97%E5%88%97%E5%87%A6%E7%90%86
   ocamlc:str.cma, ocamlopt:str.cmxa
*)
   

open Str;;

let wc () =
  let rec wc_def l_int =
    let m_string, n_bool = try read_line (), false with End_of_file -> "", true in
    print_endline (global_replace (regexp "\t") " "m_string);
    if n_bool then l_int
    else wc_def (l_int + 1)
  in
  wc_def 0
;;

Printf.printf "%d\n" (wc ());;
    
    
    
      
