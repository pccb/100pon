let wc () =
  let rec wc_def l_int =
    let m_string, n_bool = try read_line (), false with End_of_file -> "", true in
    if n_bool then l_int
    else wc_def (l_int + 1)
  in
  wc_def 0
;;

Printf.printf "%d\n" (wc ());;
    
    
    
      
