(* 
   Strモジュールを新たに読み込む必要があるので、$ ocaml 2.ml では実行できない。
   $ ocamlopt str.cmxa 2.ml -o "実行ファイル名" でコンパイルする等する必要がある
   詳しくは -> http://ocaml.jp/Chapter%2023%20The%20str%20library%EF%BC%9A%20%E6%AD%A3%E8%A6%8F%E8%A1%A8%E7%8F%BE%E3%81%A8%E6%96%87%E5%AD%97%E5%88%97%E5%87%A6%E7%90%86
   ocamlc:str.cma, ocamlopt:str.cmxa
*)
   
open Str;;

(* channel *)
let col1 = open_in "col1.txt";;
let col2 = open_in "col2.txt";;
let col3 = open_out "col3.txt";; (* col3.txtに書き込むとする *)

(* string1 ^ \t ^ string2 として書き込む関数 *)
let write_tab_write a_channel b_str_list c_str_list =
  let rec write_tab_write_sub d_str_list e_str_list =
    match (d_str_list, e_str_list) with
      ([], []) -> ignore 1
    | (f_str :: g_str_list, h_str :: i_str_list) ->
      output_string a_channel (f_str ^ "\t" ^ h_str ^ "\n");
      write_tab_write_sub g_str_list i_str_list
    | _ ->
      print_endline "error.";
      exit(1)
  in
  write_tab_write_sub b_str_list c_str_list
;;

(* openチャンネルを文字列リストに *)
let file_to_list a_channel =
  let rec file_to_list_sub () =
    let l_string, m_bool = try input_line a_channel , false with End_of_file -> "", true in
    if m_bool then []
    else l_string :: (file_to_list_sub ())
  in
  file_to_list_sub ()
;;

write_tab_write col3 (file_to_list col1) (file_to_list col2);;

(* close channel *)
close_in col1;;
close_in col2;;
close_out col3;;
      
