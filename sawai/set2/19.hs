import Control.Monad
import Text.Regex

main :: IO ()
main = do
  let
    kanji = "[\x4e00-\x9fa0]"
    num = "[0-9\xff10-\xff19]"
    city = "[あ-ん\x4e00-\x9fa0]+(区|市|町|村)"
    postal = "〒"++num++num++num++"[-ー]?"++num++num++num++num
    pref = "("++kanji++"+県|京都府|大阪府|北海道|東京都)"
    r = mkRegex $ "("++postal++")\\s*"++pref++"\\s*("++city++")"
  tweets <- liftM lines getContents
  forM_ tweets $ \t ->
    case matchRegex r t of
      Just (a:b:c:_) -> putStrLn $ a ++ "\t" ++ b ++ "\t" ++ c
      _ -> return ()
