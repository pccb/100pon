import Control.Monad
import Text.Regex

main :: IO ()
main = do
  -- [\x4e00-\x9fa0] is all kanji defined by JIS X 0208
  let r = mkRegex "([\x4e00-\x9fa0]+)[\\(（]([A-Z]+)[\\)）]"
  tweets <- liftM lines getContents
  forM_ tweets $ \t ->
    case matchRegex r t of
      Just (a:b:_) -> putStrLn $ a ++ "\t" ++ b
      _ -> return ()
