import Control.Monad
import Text.Regex.TDFA

main :: IO ()
main = do
  tweets <- liftM lines getContents
  forM_ tweets $ \t ->
    forM_ (t =~ "@[A-Za-z0-9_]+" :: [[String]]) $ putStrLn . head
