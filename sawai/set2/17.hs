import Control.Monad
import Data.List
import Text.Regex

main :: IO ()
main = do
  let kanji = "\x4e00-\x9fa0"
      suffixes = intercalate "|" ["君","くん","きゅん","さん","たん","ちゃん","殿","氏","先輩","先生","教授","選手","容疑者","被告"]
      r = mkRegex $ "(["++kanji++"]["++kanji++"]+("++suffixes++"))"
  tweets <- liftM lines getContents
  forM_ tweets $ \t ->
    case matchRegex r t of
      Just (a:_) -> putStrLn a
      _ -> return ()
