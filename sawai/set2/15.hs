import Control.Monad
import Text.Regex

main :: IO ()
main = do
  let r = mkRegex "@([A-Za-z0-9_]+)" :: Regex
      subst = "<a href=\"https://twitter.com/#!/\\1\">@\\1</a>"
  tweets <- liftM lines getContents
  forM_ tweets $ \t -> putStrLn (subRegex r t subst)
