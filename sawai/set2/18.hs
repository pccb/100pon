import Control.Monad
import Text.Regex

main :: IO ()
main = do
  let r = mkRegex $ "(仙台市[\x4e00-\x9fa0]+区[ー0-9\xff10-\xff19\x4e00-\x9fa0\\-]*)"
  tweets <- liftM lines getContents
  forM_ tweets $ \t ->
    case matchRegex r t of
      Just (a:_) -> putStrLn a
      _ -> return ()
