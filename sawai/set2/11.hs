{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO ()
main = do
  tweets <- liftM T.lines T.getContents
  let f = T.isInfixOf "拡散希望"
  forM_ (filter f tweets) T.putStrLn
