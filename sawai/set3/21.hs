{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO ()
main = do
  t <- T.getContents
  forM_ (T.lines t) $ \ls ->
    forM_ (T.split (=='.') ls) $ \s ->
      when (T.length s > 0) $ do
        T.putStr $ T.stripStart s
        T.putStrLn "."
