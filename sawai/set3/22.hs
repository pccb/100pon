import Control.Monad
import Text.Regex

main :: IO ()
main = do
  let r = mkRegex "\\. ([A-Z])" :: Regex
      subst = ".\n\\1"
  ls <- liftM lines getContents
  forM_ ls $ \l ->
    let res = subRegex r l subst :: String
    in forM_ (lines res) putStrLn
