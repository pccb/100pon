{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO()
main = do
  t <- T.getContents
  T.putStr $ T.replace "\t" " " t
