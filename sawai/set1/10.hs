{-# LANGUAGE OverloadedStrings #-}

import System.IO
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.List (sortBy)
import Data.Map (Map)
import qualified Data.Map as M

inc :: (Ord k) => k -> Map k Int -> Map k Int
inc k = M.insertWith f k 1
  where
    f old new = old + new

main :: IO ()
main = do
  h <- openFile "col2.txt" ReadMode
  ts <- liftM T.lines $ T.hGetContents h
  let
    freqMap = foldr inc M.empty ts
    compByFreq (_, v1) (_, v2) = compare v2 v1
    sortedFreqList = sortBy compByFreq (M.toList freqMap)
  forM_ sortedFreqList $ \(k, _) -> T.putStrLn k
  hClose h

