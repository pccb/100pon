{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Set as Set

main :: IO ()
main = do
  ls <- liftM T.lines T.getContents
  let ts = map (T.takeWhile (/='\t')) ls
      set = foldr Set.insert Set.empty ts
  print (Set.size set)
