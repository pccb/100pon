{-# LANGUAGE OverloadedStrings #-}

import System.Environment
import Control.Monad
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T

putFirstNLines :: [Text] -> Int -> IO ()
putFirstNLines ls n
  | n > 0 = do
      T.putStrLn $ head ls
      putFirstNLines (tail ls) (n-1)
  | otherwise = return ()

main :: IO ()
main = do
  args <- getArgs
  ls <- liftM T.lines T.getContents
  case args of
    (n:_) -> putFirstNLines ls (read n)
    _ -> return ()
