{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.List

f :: Text -> Text -> Ordering
f t1 t2 =
  let
    _:c1:_ = T.split (=='\t') t1
    _:c2:_ = T.split (=='\t') t2
  in
    compare c1 c2

main :: IO ()
main = do
  ls <- liftM T.lines T.getContents
  forM_ (sortBy f ls) T.putStrLn
