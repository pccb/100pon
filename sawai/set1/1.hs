import qualified Data.ByteString.Char8 as B

main :: IO ()
main = B.getContents >>= print . B.count '\n'
