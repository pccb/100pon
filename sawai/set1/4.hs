{-# LANGUAGE OverloadedStrings #-}

import System.IO
import Data.Monoid ((<>))
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO ()
main = do
  h1 <- openFile "col1.txt" ReadMode
  h2 <- openFile "col2.txt" ReadMode
  cols1 <- liftM T.lines $ T.hGetContents h1
  cols2 <- liftM T.lines $ T.hGetContents h2
  forM_ (zip cols1 cols2) $ \(t1, t2) ->
      T.putStrLn $ t1 <> "\t" <> t2
  hClose h1
  hClose h2

