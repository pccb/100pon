{-# LANGUAGE OverloadedStrings #-}

import System.Environment
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO ()
main = do
  args <- getArgs
  ls <- liftM T.lines T.getContents
  case args of
    (n:_) ->
      forM_ lastLs T.putStrLn
      where
        lastLs = reverse . take (read n) . reverse $ ls
    _ -> return ()
