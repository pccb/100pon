{-# LANGUAGE OverloadedStrings #-}

import System.IO
import Control.Monad
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T

extractCols :: Text -> Maybe (Text, Text)
extractCols t =
  let
    lst = T.split (=='\t') t
    f (x:y:_) = Just (x, y)
    f _ = Nothing
  in
    f lst

main :: IO ()
main = do
  h1 <- openFile "col1.txt" WriteMode
  h2 <- openFile "col2.txt" WriteMode
  t <- T.getContents
  let lst = map extractCols (T.lines t)
  forM_ lst $ \m ->
    case m of
      Just (t1, t2) -> do
        T.hPutStrLn h1 t1
        T.hPutStrLn h2 t2
      Nothing -> return ()
  hClose h1
  hClose h2

