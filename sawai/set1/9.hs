{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.List

f :: Text -> Text -> Ordering
f t1 t2 =
  let
    a1:b1:_ = T.split (=='\t') t1
    a2:b2:_ = T.split (=='\t') t2
  in
    case compare b2 b1 of
      EQ -> compare a2 a1
      comp -> comp

main :: IO ()
main = do
  ls <- liftM T.lines T.getContents
  forM_ (sortBy f ls) T.putStrLn
